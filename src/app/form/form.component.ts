import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, ValidatorFn, Validators, AbstractControl } from '@angular/forms';
import { passValidator } from '../validator';
import { Router } from '@angular/router'
import { DataService } from 'src/app/data.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  private fname: string;
  private lname: string;
  private contact: string;
  private gender: string;
  private password: string;
  private cpassword: string;
  private eid: string;
  private email: string;
  private data;
  private status = false;
  private type = "password";


  constructor(private dataService: DataService, private router: Router) {
  }
  RegistrationForm = new FormGroup({
    firstName: new FormControl('', [Validators.required, Validators.pattern('[A-Za-z]+')]),
    lastName: new FormControl('', [Validators.required, Validators.pattern('[A-Za-z]+')]),
    Gender: new FormControl('', [Validators.required, Validators.pattern('[A-Za-z]+')]),
    Contact: new FormControl('', [Validators.required, Validators.pattern('[0-9]{10}')]),
    Email: new FormControl('', [Validators.required, Validators.email]),
    Password: new FormControl('', [Validators.required, Validators.pattern('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,16}$')]),
    Cpassword: new FormControl('', [Validators.required]),
    eid: new FormControl('', [Validators.required, Validators.pattern('[0-9]{4}')])
  })

  onSubmit() {
    this.dataService.setData(this.RegistrationForm.value)
    this.router.navigate(['/display']);

  }
  ngOnInit() {
    if ((this.dataService.getData()) != null) {
      this.data = this.dataService.getData()
      this.RegistrationForm.setValue({
        firstName: this.data.firstName,
        lastName: this.data.lastName,
        Gender: this.data.Gender,
        Contact: this.data.Contact,
        Email: this.data.Email,
        Password: this.data.Password,
        Cpassword: this.data.Cpassword,
        eid: this.data.eid
      });
    }

  }

  onKey(event: any) {
    if (event.target.value === this.RegistrationForm.value.Password) {
      this.status = true;
    }
    else {
      this.status = false
    }
  }

  show() {
    this.type = "text"
  }
  hide() {
    this.type = "password"
  }
}



