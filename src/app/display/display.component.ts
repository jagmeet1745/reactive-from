import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

import { FormComponent } from '../form/form.component';
import { FormGroup, FormControl, ValidatorFn, Validators, AbstractControl } from '@angular/forms';
import { DataService } from 'src/app/data.service';
@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css']
})
export class DisplayComponent implements OnInit {
  private fname: string;
  private lname: string;
  private contact: string;
  private gender: string;
  private password: string;
  private cpassword: string;
  private eid: string;
  private email: string;
  private data;
  constructor(private router: Router,private dataService: DataService) {
    this.data = this.dataService.getData();
    this.fname = this.data.firstName;
    this.lname = this.data.lastName;
    this.contact = this.data.Contact;
    this.gender = this.data.Gender;
    this.password = this.data.Password
      ;
    this.cpassword = this.data.Cpassword;
    this.eid = this.data.eid;
    this.email = this.data.Email;
  }


  onClick() {
    this.router.navigate(['']);
  }
  ngOnInit() {
  }

}
