import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  formdata;
  constructor() { }
  setData(data){
    this.formdata=data;
  }

  getData(){
    return this.formdata;
  }
  }


