import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { FormComponent } from './form/form.component';
import { DisplayComponent } from './display/display.component';
import { RouterModule, Routes } from '@angular/router';
import { DataService } from 'src/app/data.service';
const routes: Routes = [{
  path: 'display',
  component: DisplayComponent
},
{
  path: '',
  component: FormComponent
}
]
@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    DisplayComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
